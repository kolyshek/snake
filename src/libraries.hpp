#ifndef LIBRARIES_H
#define LIBRARIES_H

#include<fstream>
#include<iostream>
#include<random>
#include<ctime>
#include<vector>
#include<cmath>
#include<algorithm>

#include<SFML/Audio.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/Network.hpp>
#include<SFML/System.hpp>
#include<SFML/Window.hpp>

#include "IBase.hpp"
#include "logging.hpp"

#endif // !LIBRARIES_H